﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using Otus.Teaching.PromoCodeFactory.Core.Infrastructure;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions
{
    public static class AppBuilderExtension
    {
        public static IApplicationBuilder Migration(this IApplicationBuilder app, DataBaseContext context)
        {
            var hasPendingMigrations = context.Database.GetPendingMigrations().Any();
            switch (hasPendingMigrations)
            {
                case true when context.Database.GetAppliedMigrations().Count() <= 1:
                    context.Database.EnsureDeleted();
                    context.Database.Migrate();

                    context.employees.AddRange(FakeDataFactory.Employees);
                    context.preferences.AddRange(FakeDataFactory.Preferences);
                    context.Customers.AddRange(FakeDataFactory.Customers);

                    context.SaveChanges();
                    break;
                case true:
                    context.Database.Migrate();
                    break;
            }

            return app;
        }
    }
}
