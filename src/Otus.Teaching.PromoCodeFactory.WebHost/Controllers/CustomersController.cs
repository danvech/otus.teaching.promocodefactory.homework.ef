﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Infrastructure;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Base;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]

    public class CustomersController
        : ControllerBase
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IRepository<Preferences> _preferenceRepository;
        private readonly DataBaseContext _dbContext;

        public CustomersController(ICustomerRepository customerRepository, DataBaseContext dbContext, IPreferenceRepository preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _dbContext = dbContext;
        }


        /// <summary>
        /// Получить список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var listCustomers = customers.Select(c =>
            new CustomerShortResponse
            {
                Id = c.Id,
                Email = c.Email,
                FirstName = c.FirstName,
                LastName = c.LastName
            }).ToList();


            return listCustomers;
        }

        /// <summary>
        /// Получить информацию о клиенте с его промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customerWithPromocodes = await _customerRepository.GetByIdIncludePromocodesAsync(id);

            var result = new CustomerResponse()
            {
                Id = customerWithPromocodes.Id,
                Email = customerWithPromocodes.Email,
                FirstName = customerWithPromocodes.FirstName,
                LastName = customerWithPromocodes.LastName,
                PromoCodes = customerWithPromocodes.PromoCodes.Select(pc => new PromoCodeShortResponse()
                {
                    Id = pc.Id,
                    BeginDate = pc.BeginDate.ToShortDateString(),
                    EndDate = pc.EndDate.ToShortDateString(),
                    Code = pc.Code,
                    PartnerName = pc.PartnerName,
                    ServiceInfo = pc.ServiceInfo
                }).ToList()
            };

            return result;
        }

        /// <summary>
        /// Создать нового клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="request">Модель пользователя</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preference = await _preferenceRepository.GetByIdsAsync(request.PreferenceIds);

            var addCustomer = new Customer()
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                PhoneNumber = request.PhoneNumber,
                DateOfBirth = request.DateOfBirth
            };

            if (preference != null)
            {
                addCustomer.CustomerPreferences = preference.Select(p => new CustomerPreference()
                {
                    IdCustomer = addCustomer.Id,
                    IdPreference = p.Id
                }).ToList();
            }

            var newId = await _customerRepository.CreateEntityAsync(addCustomer);
            return Ok(newId);
        }

        /// <summary>
        ///  Изменить существующую запись в БД
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customerInDb = await _customerRepository.GetByIdIncludePreferencesAsync(id);

            var preferencesInDb = await _preferenceRepository.GetByIdsAsync(request.PreferenceIds);

            if (customerInDb == null)
                return NotFound();


            customerInDb.FirstName = request.FirstName;
            customerInDb.LastName = request.LastName;
            customerInDb.Email = request.Email;


            if (preferencesInDb != null)
            {
                customerInDb.CustomerPreferences.Clear();
                customerInDb.CustomerPreferences = preferencesInDb.Select(p => new CustomerPreference { IdCustomer = id, IdPreference = p.Id }).ToList();
            }

            await _customerRepository.UpdateDatabaseAsync(customerInDb);

            return Ok();
        }

        /// <summary>
        /// Удаление пользователя вместе с выданными промокодами и предпочтениями
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customerInDb = await _customerRepository.GetByIdIncludeAllAsync(id);

            if (customerInDb == null)
                return NotFound();

            await _customerRepository.DeleteEntityAsync(customerInDb);

            return Ok();
        }
    }
}