﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Infrastructure;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {

        private IPromocodeRepository _repositoryPromocode;
        private IPreferenceRepository _preferenceRepository;
        private ICustomerRepository _customerRepositrory;

        public PromocodesController(IPromocodeRepository repositoryPromocode, DataBaseContext context, ICustomerRepository customerRepositrory, IPreferenceRepository preferenceRepository)
        {
            _repositoryPromocode = repositoryPromocode;
            _customerRepositrory = customerRepositrory;
            _preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var listPromocodes = await _repositoryPromocode.GetAllAsync();

            var response = listPromocodes.Select(l => new PromoCodeShortResponse
            {
                Id = l.Id,
                BeginDate = l.BeginDate.ToShortDateString(),
                EndDate = l.EndDate.ToShortDateString(),
                Code = l.Code,
                PartnerName = l.PartnerName,
                ServiceInfo = l.ServiceInfo
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferenceInDb =  await _preferenceRepository.GetPreferencesOnName(request.Preference);

            var custmoresInDb = await _customerRepositrory.CustomersGetByPreference(preferenceInDb.Id);

            if (preferenceInDb == null || custmoresInDb.Count == 0)
                return NotFound();

            var listPromocodes = new List<PromoCode>(custmoresInDb.Count);

            foreach(var customer in custmoresInDb)
            {
                listPromocodes.Add(new PromoCode
                {
                    //Id = Guid.NewGuid(),
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(7),
                    Code = "15",
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    PreferenceId = preferenceInDb.Id,
                    CustomerId = customer.Id
                }) ;
            }

            await _repositoryPromocode.CreateEntitiesAsync(listPromocodes);

            return Ok();

        }
    }
}