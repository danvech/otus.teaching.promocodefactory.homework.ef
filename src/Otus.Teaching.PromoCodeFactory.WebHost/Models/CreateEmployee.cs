﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class CreateEmployee
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ApplicationPromocodesCount { get; set; }

        public Role? role { get; set; }
    }
}
