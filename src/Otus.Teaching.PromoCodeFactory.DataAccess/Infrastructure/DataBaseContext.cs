﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Infrastructure
{
    public class DataBaseContext : DbContext
    {
        public DataBaseContext(DbContextOptions<DataBaseContext> options) : base(options)
        {

        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> employees { get; set; }
        public DbSet<Preferences> preferences { get; set; }
        public DbSet<Role> roles { get; set; }
        public DbSet<PromoCode> promoCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            #region CustomerPreference

            modelBuilder.Entity<CustomerPreference>().HasKey(cp => new { cp.IdPreference, cp.IdCustomer });

            modelBuilder.Entity<CustomerPreference>()
                .HasOne<Customer>(cp => cp.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(cp => cp.IdCustomer);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne<Preferences>(cp => cp.Preference)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(cp => cp.IdPreference);

            #endregion

            #region Customer

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.PromoCodes)
                .WithOne(pc => pc.Customer)
                .HasForeignKey(pc => pc.CustomerId);

            modelBuilder.Entity<Customer>()
                .Property(c => c.FirstName)
                .HasMaxLength(50);

            modelBuilder.Entity<Customer>()
                .Property(c => c.LastName)
                .HasMaxLength(50);

            modelBuilder.Entity<Customer>()
                .Property(c => c.Email)
                .HasMaxLength(100);

            #endregion

            #region Employee

            modelBuilder.Entity<Employee>()
                .Property(e => e.FirstName)
                .HasMaxLength(250);

            modelBuilder.Entity<Employee>()
                .Property(e => e.LastName)
                .HasMaxLength(250);

            modelBuilder.Entity<Employee>()
                .Property(e => e.Email)
                .HasMaxLength(250);

            #endregion

            #region Role

            modelBuilder.Entity<Role>()
                .Property(r => r.Description)
                .HasMaxLength(500);

            #endregion

            #region Promocodes

            modelBuilder.Entity<PromoCode>()
                .Property(pc => pc.Code)
                .HasMaxLength(250);

            modelBuilder.Entity<PromoCode>()
                .Property(pc => pc.ServiceInfo)
                .HasMaxLength(250);

            modelBuilder.Entity<PromoCode>()
                .Property(pc => pc.PartnerName)
                .HasMaxLength(100);

            #endregion

            #region Preferences

            modelBuilder.Entity<Preferences>()
                .Property(p => p.Name)
                .HasMaxLength(100);

            #endregion

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }



    }
}
