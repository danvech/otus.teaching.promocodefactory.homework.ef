﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class secondMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_promoCodes_Customers_Id",
                table: "promoCodes");

            migrationBuilder.CreateIndex(
                name: "IX_promoCodes_CustomerId",
                table: "promoCodes",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_promoCodes_Customers_CustomerId",
                table: "promoCodes",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_promoCodes_Customers_CustomerId",
                table: "promoCodes");

            migrationBuilder.DropIndex(
                name: "IX_promoCodes_CustomerId",
                table: "promoCodes");

            migrationBuilder.AddForeignKey(
                name: "FK_promoCodes_Customers_Id",
                table: "promoCodes",
                column: "Id",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
