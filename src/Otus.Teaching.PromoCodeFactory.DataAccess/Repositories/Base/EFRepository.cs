﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Base
{
    public class EFRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly DbContext Context;
        protected private DbSet<T> _entitySet;

        public EFRepository(DbContext context)
        {
            Context = context;
            _entitySet = this.Context.Set<T>();
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _entitySet.AsNoTracking().ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _entitySet.FindAsync(id);
        }

        public async Task<List<T>> GetByIdsAsync(List<Guid> listId)
        {
            return await _entitySet.Where(e => listId.Contains(e.Id)).ToListAsync();
        }

        public async Task<Guid> CreateEntityAsync(T entity)
        {
            var task = await _entitySet.AddAsync(entity);
            await Context.SaveChangesAsync();

            return task.Entity.Id;
        }

        public async Task UpdateDatabaseAsync(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            await Context.SaveChangesAsync();
        }

        public async Task SaveAsync()
        {
            await Context.SaveChangesAsync();
        }

        public async Task DeleteEntityAsync(T entity)
        {
            Context.Remove(entity);
            await Context.SaveChangesAsync();
        }

        public async Task CreateEntitiesAsync(List<T> entities)
        {
            await _entitySet.AddRangeAsync(entities);
            await Context.SaveChangesAsync();
        }
    }
}
