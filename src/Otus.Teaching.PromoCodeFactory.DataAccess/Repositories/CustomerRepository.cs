﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Infrastructure;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EFRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DbContext context) : base(context)
        {
        }

        public async Task<List<Customer>> CustomersGetByPreference(Guid id)
        {
            return await Context.Set<CustomerPreference>()
                .Include(cp => cp.Customer)
                .Where(cp => cp.IdPreference == id)
                .Select(cp => cp.Customer)
                .ToListAsync();
            
        }

        public async Task<Customer> GetByIdIncludeAllAsync(Guid id)
        {
                 return await _entitySet
                .Include(e => e.CustomerPreferences)
                .Include(e => e.PromoCodes)
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<Customer> GetByIdIncludePreferencesAsync(Guid id)
        {
            return await _entitySet
                .Include(x => x.CustomerPreferences)
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync();
        }

        public async Task<Customer> GetByIdIncludePromocodesAsync(Guid id)
        {
            return await _entitySet.Include(c => c.PromoCodes).FirstOrDefaultAsync();
        }
    }
}
