﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Infrastructure;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PreferenceRepository : EFRepository<Preferences>, IPreferenceRepository
    {
        public PreferenceRepository(DbContext Context) : base(Context)
        {
        }

        public async Task<Preferences> GetPreferencesOnName(string Name)
        {
            return await _entitySet.FirstOrDefaultAsync(p => p.Name == Name);
        }
    }
}
