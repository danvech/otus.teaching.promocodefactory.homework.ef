﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PromocodeRepository : EFRepository<PromoCode>, IPromocodeRepository
    {
        public PromocodeRepository(DbContext context) : base(context)
        {
        }

        public async Task<Customer> FindCustomer(string firstName)
        {
            var customerInDb = await Context.Set<Customer>().Where(c => c.FirstName == firstName).FirstOrDefaultAsync();
            await Context.SaveChangesAsync();

            return customerInDb;
        }
    }
}
