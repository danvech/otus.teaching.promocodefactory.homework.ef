﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T : BaseEntity
    {

        #region Get
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id);

        Task<List<T>> GetByIdsAsync(List<Guid> listId);
        #endregion

        #region Create
        Task<Guid> CreateEntityAsync(T entity);

        Task CreateEntitiesAsync(List<T> entities);

        #endregion

        #region Delete

        Task DeleteEntityAsync(T entity);

        #endregion

        #region Update

        Task SaveAsync();

        Task UpdateDatabaseAsync(T entity);

        #endregion


    }
}