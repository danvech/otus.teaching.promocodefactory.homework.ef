﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Task<Customer> GetByIdIncludePreferencesAsync(Guid id);
        Task<Customer> GetByIdIncludePromocodesAsync(Guid id);

        Task<Customer> GetByIdIncludeAllAsync(Guid id);

        Task<List<Customer>> CustomersGetByPreference(Guid id);
        

    }
}
