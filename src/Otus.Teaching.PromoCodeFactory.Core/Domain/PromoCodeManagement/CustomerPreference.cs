﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class CustomerPreference
    {
        public Guid IdCustomer { get; set; }
        public Customer Customer { get; set; }

        public Guid IdPreference { get; set; }
        public Preferences Preference { get; set; }
    }
}
