﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preferences
        :BaseEntity
    {
        public string Name { get; set; }
        public ICollection<CustomerPreference> CustomerPreferences { get; set; }
    }
}